import { Router } from 'express'
import { User } from '../model/user.model'

const router = Router()

router.post('/login', async (req, res) => {
	const { username, password } = req.body
	const user = await User.findOne({ username: username })
	if (user) {
		if (await user.authenticate(password)) {
			req.session.userId = user._id
			res.status(200).json({ message: 'welcome', success: true })
		} else {
			res.status(400).json({ message: 'wrong password', success: false })
		}
	} else {
		res.status(400).json({
			message: 'username does not exist',
			success: false
		})
	}
})

router.get('/logout', (req, res, next) => {
	if (req.session.userId) {
		req.session.destroy(err => {
			if (err) return next(err)
			return res.send()
		})
	} else {
		return res.status(400).send()
	}
})

export default router
