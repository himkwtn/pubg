import shopRouter from './shop'
import userRouter from './user'
import { Router } from 'express'

const router = Router()

router.use('/shop', shopRouter)
router.use('/user', userRouter)
export default router
