import mongoose, { Schema } from 'mongoose'

const PubgSchema = new Schema({
	item: String,
	originalPrice: Schema.Types.Mixed,
	currentPrice: Schema.Types.Mixed
})

export const Item = mongoose.model('Pubg', PubgSchema, 'Pubg')
